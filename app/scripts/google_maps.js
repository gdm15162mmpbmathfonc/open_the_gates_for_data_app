var infowindow;

function initMap() {

	var mapDiv = document.getElementById('map');
	var myLatLng = {lat: 51.055, lng: 3.72};

	var map = new google.maps.Map(mapDiv, {
		center: myLatLng,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		zoom: 13,
		scrollwheel: false
	});
	
	for (var y = 0; y < kunstenplanArray.length; y++) {
		var data = kunstenplanArray[y];
		var location = new google.maps.LatLng(data.latitude, data.longitude);
		var infoWindowContent = '<b>' + data.Naam + '</b>' + '<br>';
		infoWindowContent += data.categorie + '<br>';
		infoWindowContent += data.Straat + ' ' + data.huisnr + '<br>';
		infoWindowContent += data.Postcode + ' ' + data.Gemeente + '<br>';
		infoWindowContent += '<a href="http://' + data.website + '">' + data.website + '</a>';
		addMarker(map, infoWindowContent, location);
	}

	function addMarker(map, contentString, location) {
		var marker = new google.maps.Marker({
			position: location,
			map: map
		});

		google.maps.event.addListener(marker, 'click', function() {
			if (typeof infowindow != 'undefined') {
				infowindow.close();
			}
			infowindow = new google.maps.InfoWindow({
				content: contentString
			});
			infowindow.open(map,marker);
			map.setZoom(16);
			map.setCenter(marker.getPosition());
		});
	}

	function toggleBounce() {
		if (marker.getAnimation() !== null) {
		marker.setAnimation(null);
		} else {
		marker.setAnimation(google.maps.Animation.BOUNCE);
		}
	}
}

google.maps.event.addDomListener(window, 'load', initMap);