function initMap() {

 var mapDiv = document.getElementById('map');
 var myLatLng = {lat: 51.055, lng: 3.72};

 var map = new google.maps.Map(mapDiv, {
  center: myLatLng,
  mapTypeId: google.maps.MapTypeId.ROADMAP,
  zoom: 12
 });

 for (var y = 0; y < bibliotheekArray.length; y++) {
  var data = bibliotheekArray[y];
  var location = new google.maps.LatLng(data.latitude, data.longitude);

  addMarker(map,  location);
 }

 function addMarker(map, contentString, locatie) {
  var marker = new google.maps.Marker({
   position: location,
   map: map
  });
 }
}

google.maps.event.addDomListener(window, 'load', initMap);
