(function() {
    var App = {
        "init": function() {
            this.URLGentBibliotheek = 'http://datatank.stad.gent/4/cultuursportvrijetijd/bibliotheekopenbeschrijving.json';
            this.gentBibliotheekData = null;
            this.loadGentBibliotheek();
        },
        "loadGentBibliotheek": function() {

            var self = this;

            getJSONByPromise(this.URLGentBibliotheek).then(
                function(data) {
                    self.gentBibliotheekData = data;
                    self.updateUI();
                },
                function(status) {
                    console.log(status);
                }
            );
        },
        "updateUI": function() {
            if(this.gentBibliotheekData != null) {
                var tempStr = '', gentBibliotheek = null;
                tempStr += '<ul class="kunstenplan">';
                for (var i = 0; i < this.gentBibliotheekData.length; i++) {
                    gentBibliotheek = this.gentBibliotheekData[i];
                    tempStr += '<li class="cards">';
                    tempStr += '<span class="kunstenplan-namen">' + gentBibliotheek.title + '</span>' + '<br>';
                    tempStr += '<a href="http://' + gentBibliotheek.website + '" class="kunstenplan-link">' + gentBibliotheek.website + '</a>' + '<br>';
                    tempStr += '<span class="kunstenplan-categorie">' + gentBibliotheek.categorie + '</span>' + '<br>';
                    tempStr += '<p class="kunstenplan-beschrijving">' + gentBibliotheek.author_firstname + '</p>';
                    tempStr += '<span class="kunstenplan-btn">Toon op kaart</span>';
                    tempStr += '</li>'
                }
                tempStr += '</ul>';
                document.querySelector('.jsondata-bibliotheek').innerHTML = tempStr;
            }
        }
    };

    App.init();

})();