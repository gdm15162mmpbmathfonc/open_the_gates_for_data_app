/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *                                                                         *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     docenten WEBDAD-I
 * @created       04/02/2015
 * @modified
 * @copyright  Copyright © 2014-2015 Artevelde University College Ghent
 *
 * @function   Het script toont bij een klik op de knop de huidige
 browserversie op het scherm.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Created by thomasvandersypt on 10/08/16.
 */

function writeAddressName(latLng) {
 var geocoder = new google.maps.Geocoder();
 geocoder.geocode({
      "location": latLng
     },
     function(results, status) {
      if (status == google.maps.GeocoderStatus.OK)
       document.getElementById("address").innerHTML = results[0].formatted_address;
      else
       document.getElementById("error").innerHTML += "Unable to retrieve your address" + "<br />";
     });
}

function geolocationSuccess(position) {
 var userLatLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
 // Write the formatted address
 writeAddressName(userLatLng);

 var myOptions = {
  zoom : 12,
  center : userLatLng,
  mapTypeId : google.maps.MapTypeId.ROADMAP
 };
 // Draw the map
 var mapObject = new google.maps.Map(document.getElementById("map"), myOptions);
 // Place the marker
 new google.maps.Marker({
  map: mapObject,
  position: userLatLng
 });
 // Draw a circle around the user position to have an idea of the current localization accuracy
 var circle = new google.maps.Circle({
  center: userLatLng,
  radius: position.coords.accuracy,
  map: mapObject,
  fillColor: '#CCCC52',
  fillOpacity: 0.4,
  strokeColor: '#192B33',
  strokeOpacity: 0.7
 });
 mapObject.fitBounds(circle.getBounds());
}

function geolocationError(positionError) {
 document.getElementById("error").innerHTML += "Error: " + positionError.message + "<br />";
}

function geolocateUser() {
 // If the browser supports the Geolocation API
 if (navigator.geolocation)
 {
  var positionOptions = {
   enableHighAccuracy: true,
   timeout: 10 * 1000 // 10 seconds
  };
  navigator.geolocation.getCurrentPosition(geolocationSuccess, geolocationError, positionOptions);
 }
 else
  document.getElementById("error").innerHTML += "Your browser doesn't support the Geolocation API";
}

window.onload = geolocateUser;
