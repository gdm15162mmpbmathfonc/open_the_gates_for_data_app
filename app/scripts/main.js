
(function() {
    var App = {
        "init": function() {
            this.URLGentKunstenplan = 'http://datatank.stad.gent/4/cultuursportvrijetijd/kunstenplan.json';
            this.gentKunstenplanData = null;
            this.loadGentKunstenplan();
        },
        "loadGentKunstenplan": function() {

            var self = this;

            getJSONByPromise(this.URLGentKunstenplan).then(
                function(data) {
                    self.gentKunstenplanData = data;
                    self.updateUI();
                },
                function(status) {
                    console.log(status);
                }
            );
        },
        "updateUI": function() {
            if(this.gentKunstenplanData != null) {
                var tempStr = '', gentKunstenplan = null;
                tempStr += '<ul class="kunstenplan">';
                for (var i = 0; i < this.gentKunstenplanData.length; i++) {
                    gentKunstenplan = this.gentKunstenplanData[i];
                    tempStr += '<li class="cards">';
					tempStr += '<span class="kunstenplan-namen">' + gentKunstenplan.Naam + '</span>' + '<br>';
                    tempStr += '<a href="http://' + gentKunstenplan.website + '" class="kunstenplan-link">' + gentKunstenplan.website + '</a>' + '<br>';
                    tempStr += '<span class="kunstenplan-categorie">' + gentKunstenplan.categorie + '</span>' + '<br>';
                    tempStr += '<p class="kunstenplan-beschrijving">' + gentKunstenplan.beschrijving + '</p>';
					tempStr += '<span class="kunstenplan-btn">Scroll naar boven</span>';
                    tempStr += '</li>'
                }
                tempStr += '</ul>';
                document.querySelector('.jsondata-kunstenplan').innerHTML = tempStr;
            }
			this.toonOpKaartBtn();
        },
		"toonOpKaartBtn": function() {
			$('.kunstenplan-btn').on('click', function() {

				$('html, body').animate({scrollTop: 0}, 'slow');
			});
		}
    };

    App.init();

})();