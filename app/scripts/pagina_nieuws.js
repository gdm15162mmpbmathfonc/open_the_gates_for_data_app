
(function() {
 var App = {
  "init": function() {
   this.URLVisitGentEvents = 'http://datatank.stad.gent/4/toerisme/visitgentevents.json';
   this.visitGentEventsData = null;
   this.loadGentEvents();
  },
  "loadGentEvents": function() {

   var self = this;

   getJSONByPromise(this.URLVisitGentEvents).then(
       function(data) {
        self.visitGentEventsData = data;
        self.updateUI();
       },
       function(status) {
        console.log(status);
       }
   );
  },
  "updateUI": function() {
   if(this.visitGentEventsData != null) {
    var tempStr = '', gentEvents = null;
    tempStr += '<ul class="events">';
    for (var i = 0; i < this.visitGentEventsData.length; i++) {
        if(this.visitGentEventsData[i].language == "nl") {
            gentEvents = this.visitGentEventsData[i];
            tempStr += '<li class="cards">';
            tempStr += '<span class="events-titel">' + gentEvents.title + '</span>' + '<br>';
            tempStr += '<span class="events-image">' + '<img src="' + gentEvents.thumbs + '">' + '</span>' + '<br>';
            tempStr += '<p class="events-beschrijving">' + gentEvents.summary + '</p>';
            if (gentEvents.contact[0].website != null) {
                if (gentEvents.contact[0].website[0].url.startsWith('http://')) {
                    tempStr += '<a href="' + gentEvents.contact[0].website[0].url + '" class="events-link">' + gentEvents.contact[0].website[0].url + '</a>' + '<br>';
                } else {
                tempStr += '<a href="http:\/\/' + gentEvents.contact[0].website[0].url + '" class="events-link">' + gentEvents.contact[0].website[0].url + '</a>' + '<br>'; 
                }
            }
            tempStr += '</li>'
        }
    }
    tempStr += '</ul>';
    document.querySelector('.jsondata-gentEvents').innerHTML = tempStr;
   }
  }
 };

 App.init();

})();