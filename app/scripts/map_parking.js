var infowindow;

function initMap() {

 var mapDiv = document.getElementById('map');
 var myLatLng = {lat: 51.055, lng: 3.72};

 var map = new google.maps.Map(mapDiv, {
  center: myLatLng,
  mapTypeId: google.maps.MapTypeId.ROADMAP,
  zoom: 13
 });

 for (var y = 0; y < parkingArray.length; y++) {
  var data = parkingArray[y];
  var location = new google.maps.LatLng(data.latitude, data.longitude);
  var infoWindowContent = '<b>' + data.name + '</b>' + '<br>';
  infoWindowContent += 'Capaciteit: ' +data.totalCapacity + '<br>';
  infoWindowContent += data.address  + '<br>';
  addMarker(map, infoWindowContent, location);
 }

 function addMarker(map, contentString, locatie) {
  var marker = new google.maps.Marker({
   position: location,
   map: map
  });

  google.maps.event.addListener(marker, 'click', function() {
   if (typeof infowindow != 'undefined') {
    infowindow.close();
   }
   infowindow = new google.maps.InfoWindow({
    content: contentString
   });
   infowindow.open(map,marker);
   map.setZoom(16);
   map.setCenter(marker.getPosition());
  });
 }
}

google.maps.event.addDomListener(window, 'load', initMap);