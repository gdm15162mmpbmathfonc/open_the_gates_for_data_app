(function() {
//caching
 $btn1 = $('.button-1');
 $btn2 = $('.button-2');
 $vraag = $('.vraag');


 //1e vraag —————————————————————————————
 if($vraag.text() == "Bewegen of Relaxen?") {
  $btn1.on('click', function() {
   $vraag.html('<a class="quiz-link" href="pages/sportcentra.html">Sportcentra!</a><div id="map"><script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAxkMZglHnAH9I327BfIkxSXaO7GakaTz4&callback=initMap" async defer></script><script src="scripts/map_sport.js"></script><script src="scripts/utilities.js"></script><script src="data/sportcentra.js"></script>');

   //verberg buttons na uitkomst
   $btn1.css({
    'visibility':'hidden'
   });
   $btn2.css({
    'visibility':'hidden'
   });
  });
  $btn2.on('click', function() {
   $vraag.text('Zin in een verhaal?');
   $btn1.text('Ja');
   $btn2.text('Nee');

   //2e vraag —————————————————————————————
   if($vraag.text() == "Zin in een verhaal?") {
    $btn2.on('click', function() {
     $vraag.html('<a class="quiz-link" href="pages/kunstenplan.html">Kunstenplan!</a><div id="map"><script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAxkMZglHnAH9I327BfIkxSXaO7GakaTz4&callback=initMap" async defer></script><script src="data/kunstenplan.js"></script><script src="scripts/google_maps.js"></script> <script src="scripts/utilities.js"></script> <script src="scripts/main.js"></script>');
     //verberg buttons na uitkomst
     $btn1.css({
      'visibility':'hidden'
     });
     $btn2.css({
      'visibility':'hidden'
     });
    });
    $btn1.on('click', function() {
     $vraag.text('Lezen of Kijken?');
     //toon buttons opnieuw
     $btn1.css({
      'visibility':'visible'
     });
     $btn2.css({
      'visibility':'visible'
     });
     $btn1.text('Lezen');
     $btn2.text('Kijken');

     //3e vraag —————————————————————————————
     if($vraag.text() == 'Lezen of Kijken?') {
      $btn1.on('click', function() {
       //verberg buttons na uitkomst
       $btn1.css({
        'visibility':'hidden'
       });
       $btn2.css({
        'visibility':'hidden'
       });
       $vraag.html('<a class="quiz-link" href="pages/bibliotheek.html">Bibliotheek!</a><div id="map"><script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAxkMZglHnAH9I327BfIkxSXaO7GakaTz4&callback=initMap" async defer></script><script src="data/bibliotheek.js"></script> <script src="scripts/utilities.js"></script> <script src="scripts/map_bibliotheek.js"></script>');
      });
      $btn2.on('click', function() {
       //verberg buttons na uitkomst
       $btn1.css({
        'visibility':'hidden'
       });
       $btn2.css({
        'visibility':'hidden'
       });
       $vraag.html('<a class="quiz-link" href="pages/bioscoop.html">Bioscoop!</a><div id="map"><script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAxkMZglHnAH9I327BfIkxSXaO7GakaTz4&callback=initMap" async defer></script><script src="scripts/map_bioscoop.js"></script> <script src="scripts/utilities.js"></script> <script src="data/bioscoop.js"></script>');
      });
     }//sluiting lezen/kijken?
    });
   }//sluiting verhaal?
  });
 } //sluiting bewegen/relaxen?

})();